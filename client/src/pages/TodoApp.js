import React, { useState, useEffect } from 'react';
import TaskList from '../components/TaskList';
import TaskForm from '../components/TaskForm';
import * as taskService from '../services/taskService';

const TodoApp = () => {
    const [activeTasks, setActiveTasks] = useState([]);
    const [completedTasks, setCompletedTasks] = useState([]);

    useEffect(() => {
        fetchTasks();
    }, []);

    const fetchTasks = async () => {
        try {
            const activeResponse = await taskService.getActiveTasks();
            const completedResponse = await taskService.getCompletedTasks();
            setActiveTasks(activeResponse);
            setCompletedTasks(completedResponse);
        } catch (error) {
            console.error('Error fetching tasks:', error);
        }
    };

    const handleCompleteTask = async (task) => {
        try {
            await taskService.completeTask(task);
            await fetchTasks();
        } catch (error) {
            console.error('Error completing task:', error);
        }
    };

    const handleDeleteTask = async (task) => {
        try {
            await taskService.deleteTask(task);
            await fetchTasks();
        } catch (error) {
            console.error('Error deleting task:', error);
        }
    };

    return (
        <div className="App">
            <h1>TO DO App</h1>
            <TaskForm onAddTask={fetchTasks} />

            <h2>Active Tasks</h2>
            <TaskList
                tasks={activeTasks}
                onComplete={handleCompleteTask}
                onDelete={handleDeleteTask}
            />

            <h2>Completed Tasks</h2>
            <ul>
                {completedTasks.map((task) => (
                    <li key={task}>{task}</li>
                ))}
            </ul>
        </div>
    );
};

export default TodoApp;
