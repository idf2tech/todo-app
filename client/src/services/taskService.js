import axios from 'axios';
import apiConfig from '../config/apiConst';

const getData = async (endpoint) => {
    try {
        const response = await axios.get(`${apiConfig.API_URL}${endpoint}`);
        return response.data;
    } catch (error) {
        console.log(error);
    }
};

const postData = async (endpoint, data) => {
    try {
        await axios.post(`${apiConfig.API_URL}${endpoint}`, data);
    } catch (error) {
        console.log(error);
    }
};

export const getActiveTasks = async () => {
    return await getData('/getActiveTasks');
};

export const getCompletedTasks = async () => {
    return await getData('/getCompletedTasks');
};

export const addTask = async (task) => {
    await postData('/addTask', { task });
};

export const completeTask = async (task) => {
    await postData('/completeTask', { task });
};

export const deleteTask = async (task) => {
    await postData('/removeTask', { task });
};
