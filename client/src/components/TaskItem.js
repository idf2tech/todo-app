import React from 'react';

const TaskItem = ({ task, onComplete, onDelete }) => {
    return (
        <li>
            {task}
            <button onClick={onComplete}>Complete</button>
            <button onClick={onDelete}>Delete</button>
        </li>
    );
};

export default TaskItem;
