import React, { useState } from 'react';
import * as taskService from '../services/taskService';

const TaskForm = ({ onAddTask }) => {
    const [taskInput, setTaskInput] = useState('');

    const handleAddTask = async () => {
        try {
            await taskService.addTask(taskInput);
            onAddTask();
            setTaskInput('');
        } catch (error) {
            console.error('Error adding task:', error);
        }
    };

    return (
        <div>
            <input
                type="text"
                value={taskInput}
                onChange={(e) => setTaskInput(e.target.value)}
                placeholder="Enter a task"
            />
            <button onClick={handleAddTask}>Add Task</button>
        </div>
    );
};

export default TaskForm;
