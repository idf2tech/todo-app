import React from 'react';
import TaskItem from './TaskItem';

const TaskList = ({ tasks, onComplete, onDelete }) => {
    return (
        <ul>
            {tasks.map((task) => (
                <TaskItem
                    key={task}
                    task={task}
                    onComplete={() => onComplete(task)}
                    onDelete={() => onDelete(task)}
                />
            ))}
        </ul>
    );
};

export default TaskList;
