const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');
const taskRoutes = require('../routes/tasksRoutes');

const app = express();
let server;

const useRoutes = async () => {
  app.use('/api', taskRoutes)
};

const useMiddleware = async () => {
  app.use(cors());
  app.use(bodyParser.json());
  await useRoutes();
};

const startServer = async (port) => {
  await useMiddleware();

  server = app
    .listen(port, async () => {
      console.log(`Server is running on port ${port}`);
    })
    .on('error', (error) => {
      console.log(
        `Server running has failed on port: ${port}, error: ${error.message}`
      );
    });
};


module.exports = {
  startServer,
};
