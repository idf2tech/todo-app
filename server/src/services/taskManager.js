
class TaskManager {
    constructor() {
        this.activeTasks = [];
        this.doneTasks = [];
    }

    addTask(task) {
        this.activeTasks.push(task);
    }

    addDoneTask(task) {
        this.deleteTask(task);
        this.doneTasks.push(task);
    }

    getActiveTasks() {
        return this.activeTasks;
    }

    getDoneTasks() {
        return this.doneTasks;
    }


    deleteTask(taskToDelete) {
        this.activeTasks = this.activeTasks.filter((task) => task !== taskToDelete)
    }
}

const instance = new TaskManager();

module.exports = instance;