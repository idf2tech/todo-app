require('dotenv').config();

const config = {
  express: {
    port: parseInt(process.env.PORT, 10) || 8080,
    host: process.env.EXPRESS_HOST || 'localhost',
  },
};

config.express.url = `http://${config.express.host}:${config.express.port}`;

module.exports = config;
