const express = require('express');
const router = express.Router();
const taskManager = require('../services/taskManager')

router.post('/addTask', (req, res) => {
    taskManager.addTask(req.body.task)
    res.json(`Task added successfully`)
})

router.post('/removeTask', (req, res) => {
    taskManager.deleteTask(req.body.task)
    res.json(`Task deleted successfully`)
})

router.get('/getActiveTasks', (req, res) => {
    const tasks = taskManager.getActiveTasks()
    res.json(tasks)
})

router.get('/getCompletedTasks', (req, res) => {
    const tasks = taskManager.getDoneTasks()
    res.json(tasks)
})

router.post('/completeTask', (req, res) => {
    taskManager.addDoneTask(req.body.task)
    res.send(`Task completed`)
})
module.exports = router;