const config = require('./config');
const { startServer } = require('./services/server');

const startApp = async () => {
    try {
        await startServer(config('express:port'));
    } catch (error) {
        console.log('Failed uploading the server: ', error);
    }
};

(async () => startApp())();
